/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

/**
 *
 * @author ftonye
 */
public class BattleSea extends JFrame
{
    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    
    //Retourne un objet contenant l'adresse Internet de la machine locale.
    private InetAddress adrs;
   
    private Thread t;
   
    
    public BattleSea(BackGroundCom com)
    {
        this.setSize(580,460);
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());
        
        createGrid();
        linkListenerToSeaSector(com);       
       
         CreateShips();
       
        
    }
    
    private void createGrid()
    {
        listbtn = new JButton[100];
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setPreferredSize(new Dimension(50,30));
         listbtn[i].setBackground(java.awt.Color.CYAN);          
         this.add(listbtn[i]);
        }
        
    }
    
    private void linkListenerToSeaSector(BackGroundCom com)
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {
                com.missileOutgoing = Integer.parseInt(ae.getActionCommand());
                com.dataToSend = true;
                 t=new Thread(com);
                 t.start();
                  
             }    
          });
        }
         
    }
    
    //verification de la collision horizontal
   public boolean VerifCollision(ArrayList<JButton> Bt)
   {
        ArrayList<JButton> aShip = new ArrayList<>();
        aShip = Bt;
        
        if(listShip.size() > 0){
                for(int cptListeBateaux = 0; cptListeBateaux < listShip.size() ; cptListeBateaux++){
                     for(int cptTailleBateaux= 0; cptTailleBateaux < listShip.get(cptListeBateaux).BoatSections.size(); cptTailleBateaux++){
                        for (int j = 0 ; j < aShip.size(); j++){
                            if( listShip.get(cptListeBateaux).BoatSections.get(cptTailleBateaux) == aShip.get(j)){
                                System.out.println("Collide" );
                                return true;
                            }
                        }
                     }
                }
                return false;
        }else{
            return false;
        }
       
       /*
       int endLine=0;
       int index_position=0;
       
       index_position=head;
       while(index_position>10)
       {
           index_position-=10;
       }
       endLine=head-index_position+9;
       
       for(JButton element:Bt)
       {
           if(Integer.parseInt(element.getText())>endLine)
           {
               if(head+cpt>=2)
               {
                   break;
               }
               else
               {
                   head++;
                   break;
               }
               
           }
       }
       */
   }
   
   //verification du debordement
   public boolean VerifDebord(ArrayList<JButton> Bt)
   {
        ArrayList<JButton> aShip = new ArrayList<>();
        aShip = Bt;
        int countLine = 9;
        int outSide = 0;
        
        if (aShip.size() > 1){        
            
            for (int i = 0; i < 10; i++) {
                Bt.add(listbtn[countLine]);
                countLine = countLine + 10;
            }
            
            for (int i = 0; i < aShip.size(); i++ ){
                if(Bt.contains(aShip.get(i))){
                    outSide++;
                }
            }
            if(outSide>0)
                return false;
            else
                return true;
        }
        else{
            return true;
        }
   }
   
   //fonction qui identifie le maximum
   public boolean Maximum(int head, int cnt)
   {
       if(head+cnt>99)
       {
           return false;
       }
       else
       {
           return true;
       }
   }
   
   //fonction qui permet d'eviter qu'un beateau se place sur un autre
   public boolean IdentifierBateau(int head, int cnt, ArrayList<JButton> newBoat, ArrayList<JButton> oldBoat)
   {
       //new boat : le nouveau bateau qui va etre mis en place
       //old boat : les bateaux precedemment places
       for(JButton old:oldBoat)
       {
           for(JButton newB:newBoat)
           {
               if(old==newB)
               {
                   return false;
               }
           }
       }
       return true;
   }
   
    private void CreateShips()
    {
        int head ,nb = 0;
        boolean orientation=false;
        Random r = new Random();
        Random o = new Random();
        listShip = new ArrayList<Ship>();
        listJButtonInShip = new ArrayList<JButton>();
        
        
        for(int i = 0; i<5; i++)
        {
            try
            {
                //creer l'ensemble des boutons qui constituent le bateau
                 ArrayList<JButton> boatsection = new ArrayList<JButton>();
                 //creer aleatoirement la tete
                 head = r.nextInt(98)+1;
                 //creer la taille du bateau :  maximum c'est 4 (Si admettant que le bateau prend maximum 5 boutons)
                 nb = r.nextInt(3)+1;
                 //generer l'orientation
                 orientation= o.nextBoolean();
                 //orientation=1;
                 
                 if(!VerifCollision(boatsection) && VerifDebord(boatsection))
                 {
                     
                    if(orientation) //horizontale
                    {

                         if(nb==1)
                        {
                            for(int cnt=0; cnt<=nb; cnt++)
                            {
                                    //arraylist qui contient mes boutons de bateaux définie dans la fonction 
                                    boatsection.add(listbtn[head+cnt]);
                                    //liste de boutons qui contient la liste de mes sections de bateaux : definie globalement
                                    listJButtonInShip.add(listbtn[head+cnt]); 
                            }
                        }
                        else
                        {
                                for(int cnt=0; cnt<nb; cnt++)
                                {
                                    boatsection.add(listbtn[head+cnt]);
                                    listJButtonInShip.add(listbtn[head+cnt]); 
                                }
                            }
                            
                        }
                    else
                    {
                      if(nb==1)
                        {
                            for(int cnt=0; cnt<=nb; cnt++)
                            {
                                //arraylist qui contient mes boutons de bateaux définie dans la fonction 
                                boatsection.add(listbtn[head+cnt*10]);
                                //liste de boutons qui contient la liste de mes sections de bateaux : definie globalement
                                listJButtonInShip.add(listbtn[head+cnt*10]); 
                            }                      
                        }
                        else
                        {
                            for(int cnt=0; cnt<nb; cnt++)
                            {
                                boatsection.add(listbtn[head+cnt*10]);
                                listJButtonInShip.add(listbtn[head+cnt*10]); 
                            }
                        }
                    }
            
                 
                 
                 Ship s = new Ship(boatsection);
                 listShip.add(s);
                }     
            }
            catch(Exception ex)
            {
                
            }
           
         
            
        }
        
        
    
    }
    
    public static void UpdateGrig(int incomming)
    {
       // Boolean missHit = false;
        
        for(int i = 0; i<listbtn.length;i++)
        {
                  for(Ship element:listShip)
                  {
                      element.checkHit(listbtn[incomming]);
                     
                  }  
          
        }
     /*   
        for(JButton element:listJButtonInShip)
        {
            if(!element.equals(listbtn[incomming]))
                missHit = true;

        }
        
         if(missHit)
         {
             listbtn[incomming].setBackground(java.awt.Color.magenta); 
         }
         else
         {
             listbtn[incomming].setBackground(java.awt.Color.red); 
         }*/
    }
}
