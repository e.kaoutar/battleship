/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import javax.swing.JOptionPane;
import java.io.IOException;
import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.Socket;
/**
 *
 * @author ftonye
 */
public class BackGroundCom implements Runnable
{
     private int port;
     private String ip;
     private ServerSocket ss ;
     private Socket cs;
     private Scanner reader;
     private PrintStream writer ;
     public int missileIncomming;
     public int missileOutgoing;
     public Boolean dataToSend;
     
     public BackGroundCom(int port)
     {
         this.port = port;
         dataToSend = true;
         missileOutgoing = 100;
         startServer();
     }
     
      public BackGroundCom(int port, String ip)
     {
         this.port = port;
         this.ip=ip;
         dataToSend = true;
         missileOutgoing = 100;
         startClient();
     }
     
     private void startServer()
     {
        try 
        {   
            ss = new ServerSocket(this.port);
            cs = ss.accept();
            JOptionPane.showMessageDialog(null,"Server accept connection from" + cs.getInetAddress().getHostAddress());
        }
        catch (IOException ex)
        {
          JOptionPane.showMessageDialog(null,ex.getMessage());
        }
					 
	System.out.println("Server accept connection");
     }
     
     private void startClient()
     {
        try 
        {   
           System.out.println("Client attempting to connect ");
           cs = new Socket(this.ip, this.port);
            JOptionPane.showMessageDialog(null,"Client accept connection from" + cs.getInetAddress().getHostAddress());
            
        }
        catch (IOException ex)
        {
          JOptionPane.showMessageDialog(null,ex.getMessage());
        }
					 
	System.out.println("Client connected");
        }
     
    @Override
    public void run()
    {
        try
        {
           reader = new Scanner(cs.getInputStream());
        } 
          catch (IOException ex)
        {
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
          
        try
        {
           writer = new PrintStream(cs.getOutputStream());
        } 
        catch (IOException ex) 
        {
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
        while(true)
        {
           

          
           
          synchronized(dataToSend)
          {
             if(dataToSend)
             {
                 writer.println(missileOutgoing ); 
                 dataToSend = false;
             }
            else
             {
                 writer.println(100);
             }
          }
           
          missileIncomming = reader.nextInt();
           if(missileIncomming >= 0 && missileIncomming < 100 )
           {
               BattleSea.UpdateGrig(missileIncomming);
           }

          
             
             
        }
      
       
    }

    
}
