/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import java.util.ArrayList;
import javax.swing.JButton;

/**
 *
 * @author ftonye
 */
public class Ship 
{
    public ArrayList<JButton> BoatSections;
  
    private int status;
    private int nbHit;
    
    public Ship(ArrayList<JButton> BoatSection)
    {
        this.BoatSections = new ArrayList<JButton>();
        this.BoatSections = BoatSection;
        this.status = 0;
        this.nbHit = 0;
        initShip();
    }
    
    
   public int getStatus()
   {
       return this.status;
   }
   
   public void initShip()
   {
        int Cnt=0;
       for(JButton section:this.BoatSections)
       {
           if(Cnt==0)
           {
               section.setBackground(java.awt.Color.GRAY);
               Cnt++;
           }
           else
           {
             section.setBackground(java.awt.Color.darkGray);
           }
           
         
        }
               
    }
  
   public void checkHit(JButton cible)
   {
      
       for(JButton section:this.BoatSections)
       {
           if (section.equals(cible))
           {
             
             this.nbHit++;
             cible.setBackground(java.awt.Color.red);
            
           }
           
           else
           {
               cible.setBackground(java.awt.Color.magenta); 
           }
          
               
       }
       
       
      
   }
   
       
}
